import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.NumberFormat;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class GUI extends JFrame {

	private Image[] trainingImages = null;
	private CompressionNeuralNetwork network;
	private Image testImage;
	private int trainingSize = 0;
	private long startTime;

	private JLabel picLabel1;
	private JLabel picLabel2;

	private NeuralNetworkInfoPanel nnInfoPanel = new NeuralNetworkInfoPanel();

	private AtomicBoolean currentlyTraining = new AtomicBoolean(false);

	final JFileChooser fileChooser = new JFileChooser(".");

	public GUI() {

		super();
		trainingImages = new Image[new File("TrainingDatabase/").list().length - 1];
		
		initGUI();

		for (int i = 0; i < trainingImages.length; i++) {
			trainingImages[i] = new Image("TrainingDatabase/image" + (i+1) + ".png");
			trainingSize += (int) (trainingImages[i].size.getX() * trainingImages[i].size.getY() / 64);
			
		}

	}

	private void saveOutput(File file) {
		try {
			if (!file.toString().endsWith(".png"))
				file = new File(file.toString() + ".png");
			ImageIO.write((BufferedImage) ((ImageIcon) picLabel2.getIcon()).getImage(), "png", file);
			System.out.println("Saved.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Output file size: " + file.length()); 
	}

	private BufferedImage getImageFromArray(double[] array, int width, int height) {

		BufferedImage b = new BufferedImage(width, height, 1);

		for (int i = 0, x = 0, y = 0; i < width * height; i++) {

			int pix = (int) (array[i] * 255);

			int rgb = pix << 16 | pix << 8 | pix;
			b.setRGB(x, y, rgb);

			x++;
			if (x >= width) {
				x = 0;
				y++;
			}
		}

		return b;
	}

	private BufferedImage testNetwork(CompressionNeuralNetwork network, Image image) {

		int width = (int) image.size.getY();
		int height = (int) image.size.getX();
		double[] output = new double[(int) (height * width)];

		for (int j = 0; j < height * width / 64; j++) {
			double[] part = network.computeOutputs(image.getPostageStamp(j));
			int startPix = (8 * j) % width + width * 8 * ((8 * j) / width);
			for (int x = 0, counter = 0; x < 8; x++) {
				for (int y = 0; y < 8; y++, counter++) {
					output[startPix + y + x * width] = part[counter];
				}
			}
		}
		return getImageFromArray(output, (int) image.size.getX(), (int) image.size.getY());

	}

	private void shuffleArray(Image[] array) {
		int index;
		Image temp;
		Random random = new Random();
		for (int i = array.length - 1; i > 0; i--) {
			index = random.nextInt(i + 1);
			temp = array[index];
			array[index] = array[i];
			array[i] = temp;
		}
	}

	private CompressionNeuralNetwork createNetwork() {
		currentlyTraining.set(false);
		try {
			int numHiddenNeurons = Integer
					.parseInt(JOptionPane.showInputDialog(this, "How many hidden neurons would you like?"));
			double learningRate = Double.parseDouble(JOptionPane.showInputDialog(this,
					"What learning rate would you like? (0 to 1, 0.00008 recommended)"));
			double momentum = Double.parseDouble(
					JOptionPane.showInputDialog(this, "What momentum would you like? (0 to 1, 0.8 recommended)"));
			byte numColors = Byte.parseByte(JOptionPane.showInputDialog(this,
					"How many shades of grey would you like for the output? (2-127, with compression increasing as the number decreases. 16 recommended)"));
			return new CompressionNeuralNetwork(64, numHiddenNeurons, 64, learningRate, momentum, numColors);

		} catch (Exception e) {
			return null;
		}
	}

	private void trainNetwork(int epochs) {

		if (currentlyTraining.get()) {
			JOptionPane.showMessageDialog(GUI.this, "Already training.", "Warning", JOptionPane.WARNING_MESSAGE, null);
			return;
		}

		currentlyTraining.set(true);
		NumberFormat percentFormat = NumberFormat.getPercentInstance();
		percentFormat.setMinimumFractionDigits(4);

		startTime = System.currentTimeMillis();

		for (int i = 0; i < epochs; i++) {
			ImageIcon img = new ImageIcon(testNetwork(network, testImage));

			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					picLabel2.setIcon(img);
				}
			});

			for (int c = 0; c < trainingImages.length; c++) {
				for (int j = 0; j < trainingImages[c].size.getX() * trainingImages[c].size.getY() / 64; j++) {
					double[] postageStamp = trainingImages[c].getPostageStamp(j);
					network.computeOutputs(postageStamp);
					network.calcError(postageStamp);
					network.learn();
				}
			}
			updateInfoPanel();
			String err = percentFormat.format(network.getError(trainingSize));
			System.out.println(
					"Completed " + (i + 1) + " epochs of learning in " + (System.currentTimeMillis() - startTime) / 1000
							+ " seconds.   Error: " + err);
			nnInfoPanel.setNetworkError(err);
			network.incrementEpoch();
			shuffleArray(trainingImages);
		}
		ImageIcon img = new ImageIcon(testNetwork(network, testImage));

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				picLabel2.setIcon(img);
			}
		});
		currentlyTraining.set(false);

	}

	private void initGUI() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String testImagePath = "test5.png";
				JPanel panel = new JPanel(new WrapLayout());
				picLabel1 = new JLabel();
				picLabel2 = new JLabel();
				setTestImage(testImagePath);
				panel.add(picLabel1);
				panel.add(picLabel2);

				JButton trainButton = new JButton("Train");
				trainButton.addActionListener(onTrainButtonPressed);
				panel.add(trainButton);

				JButton saveNetworkButton = new JButton("Save network");
				saveNetworkButton.addActionListener(onSaveNetworkButtonPressed);
				panel.add(saveNetworkButton);

				JButton loadNetworkButton = new JButton("Load network");
				loadNetworkButton.addActionListener(onLoadNetworkButtonPressed);
				panel.add(loadNetworkButton);

				JButton newNetworkButton = new JButton("New network");
				newNetworkButton.addActionListener(onNewNetworkButtonPressed);
				panel.add(newNetworkButton);

				JButton saveImageButton = new JButton("Save image");
				saveImageButton.addActionListener(onSaveImageButtonPressed);
				panel.add(saveImageButton);

				JButton chooseTestImageButton = new JButton("Choose test image");
				chooseTestImageButton.addActionListener(onChooseTestImageButtonPressed);
				panel.add(chooseTestImageButton);

				panel.add(nnInfoPanel);

				add(panel);
				setVisible(true);
				setPreferredSize(new Dimension(testImage.size.x * 2 + 25, 800));
				pack();
			}
		});
	}

	ActionListener onTrainButtonPressed = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (network == null) {
				JOptionPane.showMessageDialog(GUI.this, "Please load or create a network before training.", "Error",
						JOptionPane.ERROR_MESSAGE, null);
				return;
			}

			try {
				int numEpochs = Integer
						.parseInt(JOptionPane.showInputDialog(GUI.this, "How many epochs of training would you like?"));
				if (numEpochs > 0)
					new Thread(new Runnable() {

						@Override
						public void run() {
							trainNetwork(numEpochs);
						}
					}).start();
			}

			catch (NumberFormatException ex) {

			}
		}

	};

	ActionListener onSaveImageButtonPressed = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (network == null) {
				JOptionPane.showMessageDialog(GUI.this, "Please load or create a network before saving.", "Error",
						JOptionPane.ERROR_MESSAGE, null);
				return;
			}
			int dialogRetVal = fileChooser.showSaveDialog(GUI.this);

			switch (dialogRetVal) {
			case JFileChooser.APPROVE_OPTION:
				saveOutput(fileChooser.getSelectedFile());
				break;
			case JFileChooser.ERROR_OPTION:
				JOptionPane.showMessageDialog(GUI.this, "Failed to save file.", "Error", JOptionPane.ERROR_MESSAGE,
						null);
			case JFileChooser.CANCEL_OPTION:
			default:
				break;
			}

		}
	};

	ActionListener onSaveNetworkButtonPressed = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {

			if (network == null) {
				JOptionPane.showMessageDialog(GUI.this, "Please load or create a network before saving.", "Error",
						JOptionPane.ERROR_MESSAGE, null);
				return;
			}
			int dialogRetVal = fileChooser.showSaveDialog(GUI.this);

			switch (dialogRetVal) {
			case JFileChooser.APPROVE_OPTION:
				saveNetwork(fileChooser.getSelectedFile().toString());
				break;
			case JFileChooser.ERROR_OPTION:
				JOptionPane.showMessageDialog(GUI.this, "Failed to save file.", "Error", JOptionPane.ERROR_MESSAGE,
						null);
			case JFileChooser.CANCEL_OPTION:
			default:
				break;
			}

		}
	};

	ActionListener onLoadNetworkButtonPressed = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {

			int dialogRetVal = fileChooser.showOpenDialog(GUI.this);

			switch (dialogRetVal) {
			case JFileChooser.APPROVE_OPTION:
				loadNetwork(fileChooser.getSelectedFile().toString());
				break;
			case JFileChooser.ERROR_OPTION:
				JOptionPane.showMessageDialog(GUI.this, "Failed to open file.", "Error", JOptionPane.ERROR_MESSAGE,
						null);
			case JFileChooser.CANCEL_OPTION:
			default:
				break;
			}

		}
	};

	ActionListener onNewNetworkButtonPressed = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {

			CompressionNeuralNetwork temp = createNetwork();
			if (temp != null) {
				network = temp;
				updateInfoPanel();
			}
		}
	};

	ActionListener onChooseTestImageButtonPressed = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			int dialogRetVal = fileChooser.showOpenDialog(GUI.this);

			switch (dialogRetVal) {
			case JFileChooser.APPROVE_OPTION:
				setTestImage(fileChooser.getSelectedFile().toString());
				break;
			case JFileChooser.ERROR_OPTION:
				JOptionPane.showMessageDialog(GUI.this, "Failed to open file.", "Error", JOptionPane.ERROR_MESSAGE,
						null);
			case JFileChooser.CANCEL_OPTION:
			default:
				break;
			}
		}
	};

	private void saveNetwork(String path) {
		try {
			if (!path.toString().endsWith(".ser"))
				path += ".ser";
			FileOutputStream fileOut = new FileOutputStream(path);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(network);
			out.close();
			fileOut.close();
			System.out.printf("Network saved in '" + path + "'");
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	private void loadNetwork(String path) {
		try {
			FileInputStream fileIn = new FileInputStream(path);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			network = (CompressionNeuralNetwork) in.readObject();
			updateInfoPanel();
			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
		} catch (ClassNotFoundException c) {
			System.out.println("CompressionNeuralNetwork class not found");
			c.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateInfoPanel() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				nnInfoPanel.updateDisplay(network);
				pack();
			}
		});
	}

	private void setTestImage(String path) {
		File file = new File(path);
		nnInfoPanel.setTestImageSize(file.length());
		testImage = new Image(path);
		picLabel1.setIcon(new ImageIcon(path));
		picLabel2.setPreferredSize(picLabel1.getPreferredSize());
		setPreferredSize(new Dimension(testImage.size.x * 2 + 25, testImage.size.y + 300));

		repaint();
	}
	
	
	@Override
	public void setPreferredSize(Dimension size)
	{
		super.setPreferredSize(size);
		nnInfoPanel.setPreferredSize(new Dimension(size.width, 150));
	}
}
