Compresses images using an artificial neural network (ANN), and displays the result to a user, along with diagnostic information.

Allows custom ANNs, saving/loading of ANNs, training, user selectable test images, and overall ANN error display. Output images can be saved as a PNG.

A binary with the proper file structure is available for download.