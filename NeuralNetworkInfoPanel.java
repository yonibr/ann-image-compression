import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class NeuralNetworkInfoPanel extends JPanel {

	JLabel hiddenNeurons = new JLabel(""), learningRate = new JLabel(""), momentum = new JLabel(""),
			epoch = new JLabel(""), numColors = new JLabel(""), testImageSize = new JLabel(""), networkError = new JLabel("");

	public NeuralNetworkInfoPanel() {
		super();
		this.add(hiddenNeurons);
		this.add(learningRate);
		this.add(momentum);
		this.add(numColors);
		this.add(epoch);
		this.add(testImageSize);
		this.add(networkError);
	}

	public void updateDisplay(CompressionNeuralNetwork network) {
		hiddenNeurons.setText("Hidden neurons: " + network.hiddenCount);
		learningRate.setText("Learning rate: " + network.learnRate);
		momentum.setText("Momentum: " + network.momentum);
		numColors.setText("Number of colors: " + network.numColors);
		epoch.setText("Epoch: " + network.epoch);
		revalidate();
	}
	
	public void setNetworkError(String error)
	{
		networkError.setText("Network Error:  " + error);
	}
	
	public void setTestImageSize(long size) 
	{
		testImageSize.setText("Test image size: " + size + "bytes");
	}
}
