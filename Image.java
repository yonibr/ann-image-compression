import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

public class Image {
	double[] pixelData;
	Point size = new Point();

	public Image(String path) {
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		size.x = image.getHeight();
		size.y = image.getWidth();
		pixelData = convert2(image);
	}

	double[] getPostageStamp(int index) {
		double[] postageStamp = new double[64];
		int startPixel = (8 * index) % size.y + size.y * 8 * ((8 * index) / size.y);;
		for (int y = 0, counter = 0; y < 8; y++)
			for (int x = 0; x < 8; x++, counter++) {
				postageStamp[counter] = pixelData[startPixel + x + y * size.y];
			}

		return postageStamp;
		// return Arrays.copyOfRange(pixelData, index * 64, index * 64 + 64);
	}

	private static double[] convert(BufferedImage image) {

		final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		final int width = image.getWidth();
		final int height = image.getHeight();
		final boolean hasAlphaChannel = image.getAlphaRaster() != null;

		double[] result = new double[height * width];
		if (hasAlphaChannel) {
			final int pixelLength = 4;
			for (int pixel = 0; pixel < result.length - 4; pixel += pixelLength) {
				result[pixel] = (pixels[pixel + 1] & 0xff) / 255.0 * 0.0722; // Blue
				result[pixel] += (pixels[pixel + 2] & 0xff) / 255.0 * 0.7152; // Green
				result[pixel] += (pixels[pixel + 3] & 0xff) / 255.0 * 0.2126; // Red

			}
		} else {
			final int pixelLength = 3;
			for (int pixel = 0; pixel < result.length - 3; pixel += pixelLength) {
				result[pixel] = (pixels[pixel] & 0xff) / 255.0 * 0.0722; // Blue
				result[pixel] += (pixels[pixel + 1] & 0xff) / 255.0 * 0.7152; // Green
				result[pixel] += (pixels[pixel + 2] & 0xff) / 255.0 * 0.2126; // Red
			}
		}

		return result;
	}

	private static double[] convert2(BufferedImage image) {
		int width = image.getWidth();
		int height = image.getHeight();
		double[] result = new double[height * width];

		for (int row = 0, counter = 0; row < height; row++) {
			for (int col = 0; col < width; col++, counter++) {
				result[counter] = (image.getRGB(col, row) & 0xFFFFFF) / (double) (255 * 255 * 255);
			}
		}

		return result;
	}
}
